cbuffer cbPerObject
{
	float4x4 WVP;
};

struct VS_INPUT
{
    float4 pos : POSITION;
    float3 nor : NORMAL;
    float2 tex : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD;
};

VS_OUTPUT VS(VS_INPUT input)
{
    VS_OUTPUT output;
	
    output.pos = mul(input.pos, WVP);
    output.tex = input.tex;

    return output;
}
#include "MyRender.h"

MyRender::MyRender()
{
	_pMyObj = nullptr;
}

bool MyRender::Init()
{
	XMVECTOR camPosition = XMVectorSet(0.0f,6.0f, -8.0f, 0.0f);
	XMVECTOR camTarget = XMVectorSet(0.0f, 3.0f, 0.0f, 0.0f);
	XMVECTOR camUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	_camView = XMMatrixLookAtLH(camPosition, camTarget, camUp);


	_pMyObj = new OBJ(this);
	if (!_pMyObj->Init(L"cactus")) { return false; }
	return true;
}

bool MyRender::Draw()
{
	_pMyObj->Draw(_camView);
	return true;
}

void MyRender::Close()
{
	_DELETE(_pMyObj);
}